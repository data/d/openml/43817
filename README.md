# OpenML dataset: English-Premier-League-Data-2009---2019

https://www.openml.org/d/43817

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context

As the EPL season is put on hold due to COVID-19, let us look into the data from the past 10 seasons.
We have data from 3800 matches which could give interesting insights about the English Premier League and the teams.
Content
Every match in the 10 seasons from 2009 till 2019 is listed with the following details.
Div - Division (E0)    
Date - Date of the match
HomeTeam
AwayTeam
FTHG - Full Time Goal of Home Team
FTAG - Full Time Goal of Away Team
FTR - Full Time Result (H - Home side win, A - Away side win, D - Draw)
HTHG - Half Time Goal of Home Team
HTAG - Half Time Goal of Away Team
HTR - Half Time Result (H - Home side win, A - Away side win, D - Draw)
Referee - Name of the referee who officiated the match
HS - Total shots made by home team
AS - Total shots made by away team
HST    - Total shots on target made by home team
AST    - Total shots on target made by away team
HF - Total fouls committed by home team
AF - Total fouls committed by away team
HC - Total corners received for home team (conceded by away team)
AC - Total corners received for away team (conceded by home team)
HY - Total yellow cards received for home team
AY - Total yellow cards received for away team
HR - Total red cards received for home team
AR - Total red cards received for away team
Acknowledgements
This data was created from the files downloaded from the link below.
https://www.football-data.co.uk/englandm.php
Inspiration
Try to answer the following questions.

How many matches were played in EPL between 2009 - 2019?
  How many teams played in the EPL during these 10 seasons?
  Which are they?
How many games were played by each team in these 10 seasons?
Which teams played all the seasons in EPL from 2009 - 2019?
  Which teams played only 1 season in EPL between 2009 - 2019?
How much percent of the total matches were won by the home team, away team or draw?
How many referees officiated the EPL matches between 2009 and 2019?
  List them with the total number of matches officiated.
In how many games did the teams loosing at half time come back to win the game at full time (comeback wins)? List all of them.
Sort the comeback wins based on year.
Sort the comeback wins based on season.
Generate a data frame with the following info,
a. Number of games per team.
b. Home wins per team.
c. Home defeats per team.
d. Away wins per team.
e. Away defeats per team.
f. Number of draw per teams (home and away combined).
g. Total points in EPL across 10 seasons.
Extract the total points gained by teams who played in all 10 seasons.
Which game/games produced most number of goals? List them.
Find out the total goals scored by each team at home, away and in total.
  Find the average goals scored per game at home and away (not combined).
Find the total number of dominant performances by each team. (Winning by a goal margin of 3 or more goals at Full Time).
Find the total points collected by each team per season. Extract the points table of the teams who played all 10 seasons.
Find the total shots on goal (shots and shots on target separately) made by each team.
Extract the shots and goals stats of the teams who played all 10 seasons.
Find the conversion rates (goals/shots) for the teams which played all 10 seasons.
Find the average goals conceded home and away for all the teams.
Find the total yellow and red cards given to home and away teams.
Find the total yellow and red cards given to each team home and away.
Find the total number of yellow and red cards awarded by each referee.
Find the total number of fouls committed by each team  home and away.
Find the yellow card per foul in  and red card per foul in  for every team.
Find the corners gained/conceded during home and away games for each team.
  Also find the total corners gained and conceded during 2009 - 2019.
Find the EPL champions of each year with their points.
  If 2 teams have equal points in a season, find the champion by goal difference. (Total Goals Scored - Total Goals Conceded)
  Find how many teams became EPL champions during 2009 - 2019. List their names.
  Find the team which won the EPL more times between 2009 - 2019.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43817) of an [OpenML dataset](https://www.openml.org/d/43817). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43817/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43817/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43817/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

